'use strict';

var gulp = require('gulp');
var del = require('del');
var rename = require("gulp-rename");
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');

gulp.task('lint', function() {
    return gulp.src(['./public/scripts/**/*.js',
        '!./public/scripts/main.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('copy', ['clean'], function () {
    gulp.src(['bower_components/angular/angular.*',
        'bower_components/angular-resource/angular-resource.*',
        'bower_components/angular-route/angular-route.*',
        'bower_components/angular-animate/angular-animate.*',
        'bower_components/angular-messages/angular-messages.*',
        'bower_components/angular-cookies/angular-cookies.*',
        'bower_components/angular-touch/angular-touch.*',
        'bower_components/angular-aria/angular-aria.*',
        'bower_components/angular-sanitize/angular-sanitize.*',
        'bower_components/angulartics/dist/angulartics.min.js',
        'bower_components/angulartics/dist/angulartics-ga.min.js',
        'bower_components/lodash/lodash.js',
        'bower_components/lodash/lodash.min.js',
        'bower_components/momentjs/moment.js',
        'bower_components/momentjs/min/moment.min.js',
        'bower_components/angular-ui-bootstrap-bower/ui-bootstrap-*.js'])
        .pipe(gulp.dest('./public/lib/'));

    //bootstrap fonts
    gulp.src('bower_components/bootstrap-sass/assets/fonts/**')
        .pipe(gulp.dest('./public/fonts/'));

    //bootstrap styles
    gulp.src('bower_components/bootstrap-sass/assets/stylesheets/bootstrap/**')
        .pipe(gulp.dest('./public/css/bootstrap/'));
});

gulp.task('clean', function (cb) {
    return del(['/css/project.css'], cb);
});

gulp.task('sass', [], function() {
    gulp.src(['./public/css/*.scss'])
        .pipe(sass())
        .pipe(rename('project.css'))
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('watch', function() {
    gulp.watch('./public/css/*.scss', ['sass']);
});

gulp.task('default', ['watch'], function() {

});
