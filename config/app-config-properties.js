module.exports = {
  appTitle: process.env.SITE_TITLE || 'ng-learn',
  serverPort: process.env.PORT || '3000'
};
